import React from 'react';
import {
  View,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  Image,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
} from 'react-native';

const Detail = ({navigation, route}) => {
  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{paddingBottom: 10}}>
        <Image
          source={require('../assets/image/mln.png')}
          style={{
            width: Dimensions.get('window').width,
            height: 300,
          }}
        />
        <View
          style={{
            width: '100%',
            backgroundColor: '#fff',
            borderTopLeftRadius: 19,
            borderTopRightRadius: 19,
            paddingHorizontal: 20,
            paddingTop: 38,
            marginTop: -20,
          }}></View>
        <Text
          style={{
            left: 22,
            fontFamily: 'Montserrat',
            fontStyle: 'normal',
            fontWeight: '700',
            fontSize: 25,
            lineHeight: 33,
            color: '#0a0827',
          }}>
          Jack Repair Seturan
        </Text>
        <Image
          source={require('../assets/icon/RatingB4.png')}
          style={{
            width: 70,
            height: 13.5,
            left: 25,
            marginTop: 10,
          }}
        />
        <Text
          style={{
            height: 20,
            width: 55,
            fontFamily: 'Montserrat',
            fontSize: 10,
            fontStyle: 'normal',
            fontWeight: '500',
            lineHeight: 20,
            color: '#d8d8d8',
            left: 27,
          }}>
          4.8 Ratings
        </Text>
        <Text
          style={{
            fontSize: 13,
            color: '#333',
            paddingLeft: 100,
            paddingRight: 70,
            // textAlign: 'center',
            right: 60,
            bottom: 6.5,
            margin: 10,
          }}>
          Jl. Jendral Sudirman No. 2, Kec. Sleman, Yogyakarta. 50229
        </Text>
        <Image
          source={require('../assets/icon/Lokasi.png')}
          style={{
            width: 16,
            height: 20,
            left: 25,
            marginTop: -40,
          }}
        />
        <TouchableOpacity>
          <Text
            style={{
              fontSize: 13,
              color: 'blue',
              //   paddingRight: 18,
              right: 15,
              textAlign: 'right',
              top: -18,
              fontFamily: 'Montserrat',
            }}>
            Google Maps
          </Text>
        </TouchableOpacity>
        <Text
          style={{
            fontWeight: 'bold',
            fontSize: 16,
            color: '#333',
            paddingLeft: 100,
            paddingRight: 10,
            textAlign: 'center',
            right: 100,
            margin: 10,
            top: -7,
          }}>
          09:00 - 21:00 PM
        </Text>
        <View
          style={{
            width: 58,
            height: 21,
            left: 20,
            borderRadius: 10,
            backgroundColor: '#11a84e',
            opacity: 0.4,
            justifyContent: 'center',
            alignItems: 'center',
            top: -36,
          }}>
          <Text
            style={{
              width: 43,
              height: 15,
              fontFamily: 'Montserrat',
              fontSize: 12,
              fontStyle: 'normal',
              fontWeight: '700',
              lineHeight: 16,
              letterSpacing: 1,
              color: 'green',
              left: 7,
            }}>
            OPEN
          </Text>
        </View>
        <View>
          <Text
            style={{
              left: 22,
              fontFamily: 'Montserrat',
              fontStyle: 'normal',
              fontWeight: '500',
              fontWeight: 'bold',
              fontSize: 16,
              lineHeight: 17.21,
              color: '#0a0827',
            }}>
            Deskripsi
          </Text>
          <Text
            style={{
              fontSize: 14,
              color: '#333',
              paddingLeft: 100,
              //   paddingRight: 0,
              textAlign: 'left',
              right: 87,
              margin: 10,
            }}>
            Di masa kini, sepatu terbuat dalam berbagai bahan dan beraneka model
            serta warna. Setiap dari kita memiliki sepatu sesuai kesukaan dan
            kegunaan masing-masing. Ada sepatu olahraga, sepatu untuk bekerja,
            sepatu untuk aktivitas santai, sepatu untuk sekolah, dan lain
            sebagainya. Harga sepatu juga sangat bervariasi. Dari yang cukup
            terjangkau hingga yang harganya wow lantaran berstatus edisi
            terbatas (hanya diproduksi dalam jumlah terbatas, terbuat dari bahan
            kualitas nomor satu, serta sifatnya eksklusif). Satu di antara
            kata-kata terkenal yang 'melibatkan' sepatu, yang mungkin sudah
            sering kamu dengar ialah 'sepasang sepatu yang bagus akan membawamu
            ke tempat yang bagus'.
          </Text>
          <Text
            style={{
              left: 22,
              fontFamily: 'Montserrat',
              fontStyle: 'normal',
              fontWeight: '500',
              fontWeight: 'bold',
              fontSize: 16,
              lineHeight: 17.21,
              color: '#0a0827',
              marginTop: 10,
            }}>
            Range Biaya
          </Text>
          <Text
            style={{
              fontSize: 16,
              color: '#333',
              paddingLeft: 100,
              paddingRight: 10,
              textAlign: 'left',
              right: 87,
              margin: 10,
            }}>
            Rp 20.000 - Rp 100.000
          </Text>
          <TouchableOpacity
            onPress={() => navigation.navigate('Form')}
            style={{
              width: '80%',
              marginTop: 50,
              backgroundColor: '#BB2427',
              borderRadius: 10,
              paddingVertical: 15,
              justifyContent: 'center',
              alignItems: 'center',
              left: 38,
            }}>
            <Text
              style={{
                color: '#fff',
                fontSize: 20,
                fontWeight: 'bold',
              }}>
              Repair Disini
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};

export default Detail;
