import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Login from './screen/ScreenLogin';
import Register from './screen/ScreenRegister';
import Reset from './screen/ScreenLupaPassword';
import PhoneQuestion from './screen/ScreenPhoneQuestion';
import Home from './screen/ScreenHome';
import Detail from './screen/Detail';
import Form from './screen/Form';

const Stack = createNativeStackNavigator();

export default function AuthNavigation() {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="Reset" component={Reset} />
      <Stack.Screen name="Register" component={Register} />
      <Stack.Screen name="PhoneQuestion" component={PhoneQuestion} />
      <Stack.Screen name="Home" component={Home} />
      <Stack.Screen name="Detail" component={Detail} />
      <Stack.Screen name="Form" component={Form} />
    </Stack.Navigator>
  );
}
