import React from 'react';
import {
  View,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  Image,
  TextInput,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

const Dashboard = () => {
  return (
    <View style={{flex: 1, backgroundColor: '#f8f8f8'}}>
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{paddingBottom: 10}}>
        <KeyboardAvoidingView
          behavior="padding"
          enabled
          keyboardVerticalOffset={-500}>
          <View
            style={{
              width: Dimensions.get('screen').width,
              height: 275,
              backgroundColor: '#fff',
            }}>
            <View
              style={{
                width: '100%',
                flexDirection: 'row',
                alignItems: 'center',
                marginTop: 56,
                justifyContent: 'space-between',
                paddingLeft: 22,
                paddingRight: 34,
              }}>
              <TouchableOpacity>
                <Image
                  source={require('../assets/image/arimakana.jpg')}
                  style={{
                    width: 45,
                    height: 45,
                    resizeMode: 'contain',
                  }}
                />
              </TouchableOpacity>
              <TouchableOpacity>
                <Image
                  source={require('../assets/icon/Notification.png')}
                  style={{
                    width: 24,
                    height: 24,
                    resizeMode: 'contain',
                  }}
                />
              </TouchableOpacity>
            </View>
          </View>
        </KeyboardAvoidingView>
      </ScrollView>
    </View>
  );
};

export default Dashboard;

const style = StyleSheet.create({});
