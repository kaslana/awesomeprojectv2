import React, {useState} from 'react';
import {
  View,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  Image,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
} from 'react-native';

const PhoneQuestion = ({navigation, route}) => {
  const [number, setNumber] = useState('');

  const handleInputChange = text => {
    // hanya menerima angka
    setNumber(text.replace(/[^0-9]/g, ''));
};
  return (
    <View style={styles.container}>
      <ScrollView 
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{paddingBottom: 10}}>
        <KeyboardAvoidingView
          behavior="padding"
          enabled
          keyboardVerticalOffset={-500}>
          <Image
            source={require('../assets/image/mln.png')}
            style={styles.background}
          />
          <View style={styles.input}>
            <Text style={styles.title}>Do You Want To Add Phone Number?</Text>
            <TextInput
              placeholder="+62"
              style={styles.phoneInput}
              keyboardType='numeric'
              onChangeText={handleInputChange}
              value={number}
            />
          </View>
          <TouchableOpacity
            style={styles.nextButton}
            onPress={() => navigation.navigate('Home')}>
            <Text style={styles.nextText}>Next</Text>
          </TouchableOpacity>
          <View style={styles.nextNavigate}>
            <Text style={styles.nextQuestion}>Don't add my Phone number?</Text>
            <TouchableOpacity onPress={() => navigation.navigate('Home')}>
              <Text style={styles.nextText2}>Login</Text>
            </TouchableOpacity>
          </View>
        </KeyboardAvoidingView>
      </ScrollView>
    </View>
  );
};

export default PhoneQuestion;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  background: {
    width: Dimensions.get('window').width,
    height: 317,
  },
  title: {
    fontSize: 30,
    fontWeight: 'bold',
    color: '#0A0827',
  },
  input: {
    width: '100%',
    backgroundColor: '#fff',
    borderTopLeftRadius: 19,
    borderTopRightRadius: 19,
    paddingHorizontal: 20,
    paddingTop: 38,
    marginTop: -20,
  },
  phoneInput: {
    marginTop: 11,
    width: '100%',
    borderRadius: 8,
    backgroundColor: '#F6F8FF',
    paddingHorizontal: 10,
  },
  nextButton: {
    width: '90%',
    marginLeft: 20,
    marginTop: 15,
    backgroundColor: '#335EFF',
    borderRadius: 8,
    paddingVertical: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },
  nextText: {
    color: '#fff',
    fontSize: 16,
    fontWeight: 'bold',
  },
  nextNavigate: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 30,
    flexDirection: 'row',
  },
  nextQuestion: {
    fontSize: 12,
    color: '#717171',
  },
  nextText2: {
    fontSize: 14,
    color: '#335EFF',
    marginLeft: 5,
  },
});
