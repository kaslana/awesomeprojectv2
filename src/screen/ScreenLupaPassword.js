import React from 'react';
import {
  View,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  Image,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
} from 'react-native';

const Reset = ({navigation, route}) => {
  return (
    <View style={styles.container}>
      <ScrollView //component yang digunakan agar tampilan kita bisa discroll
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{paddingBottom: 10}}>
        <KeyboardAvoidingView //component yang digunakan untuk mengatur agar keyboard tidak menutupi
          behavior="padding" //tampilan form atau text input
          enabled
          keyboardVerticalOffset={-500}>
          <Image
            source={require('../assets/image/BackgroundPhoto.png')} //load atau panggil asset image dari local
            style={styles.background}
          />
          <View style={styles.input}>
            <Text style={styles.title}>Reset Password</Text>
            <Text style={styles.email}>Email</Text>
            <TextInput //component yang digunakan untuk memasukkan data yang kita inginkan
              caretHidden={false}
              placeholder="Masukkan Email" //pada tampilan ini, kita ingin user memasukkan email
              style={styles.emailInput}
              keyboardType="email-address" //akan muncul tombol @ pada keyboard yang nanti akan memudahkan user mengisi email
            />
            <Text style={styles.password}>New Password</Text>
            <TextInput //component yang digunakan untuk memasukkan data password
              placeholder="Masukkan Password Baru"
              secureTextEntry={true} //props yang digunakan untuk menyembunyikan password user
              style={styles.passwordInput}
            />
            <TouchableOpacity
              style={styles.resetButton}
              onPress={() => navigation.navigate('Login')}>
              <Text style={styles.resetText}>Reset</Text>
            </TouchableOpacity>
            <View style={styles.loginNavigate}>
              <Text style={styles.loginQuestion}>Have An Account yet?</Text>
              <TouchableOpacity onPress={() => navigation.navigate('Login')}>
                <Text style={styles.loginText}>Login</Text>
              </TouchableOpacity>
            </View>
          </View>
        </KeyboardAvoidingView>
      </ScrollView>
    </View>
  );
};

export default Reset;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  background: {
    width: Dimensions.get('window').width, //atur agar lebar gambar adalah selebar layar device
    height: 317,
  },
  title: {
    fontSize: 30,
    fontWeight: 'bold',
    color: '#0A0827',
  },
  input: {
    width: '100%',
    backgroundColor: '#fff',
    borderTopLeftRadius: 19,
    borderTopRightRadius: 19,
    paddingHorizontal: 20,
    paddingTop: 38,
    marginTop: -20,
  },
  email: {
    color: 'red',
    fontWeight: 'bold',
    marginTop: 25,
  },
  emailInput: {
    marginTop: 11,
    width: '100%',
    borderRadius: 8,
    backgroundColor: '#F6F8FF',
    paddingHorizontal: 10,
  },
  password: {
    color: 'red',
    fontWeight: 'bold',
    marginTop: 11,
  },
  passwordInput: {
    marginTop: 11,
    width: '100%',
    borderRadius: 8,
    backgroundColor: '#F6F8FF',
    paddingHorizontal: 10,
  },
  resetButton: {
    width: '100%',
    marginTop: 77,
    backgroundColor: '#BB2427',
    borderRadius: 8,
    paddingVertical: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },
  resetText: {
    color: '#fff',
    fontSize: 16,
    fontWeight: 'bold',
  },
  loginNavigate: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 30,
    flexDirection: 'row',
  },
  loginQuestion: {
    fontSize: 12,
    color: '#717171',
  },
  loginText: {
    fontSize: 14,
    color: '#BB2427',
    marginLeft: 5,
  },
});
