import React, {useEffect} from 'react';
import {View, Image} from 'react-native';

const ScreenLoading = ({navigation, route}) => {
  useEffect(() => {
    setTimeout(() => {
      navigation.navigate('AuthNavigation');
    }, 1000);
  }, []);

  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: '#fff'}}>
      <Image
        source={require('../assets/image/Logo.png')}
        style={{width: 150, resizeMode: 'contain'}}
      />
    </View>
  );
};

export default ScreenLoading;
