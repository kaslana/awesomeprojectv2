import React from 'react';
import {
  View,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  Image,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
} from 'react-native';

const Login = ({navigation, route}) => {
  return (
    <View style={styles.container}>
      {/* <ScrollView
            showsVerticalScrollIndicator={false}
            contentContainerStyle={{paddingBottom: 10}}
         > */}
      <KeyboardAvoidingView
        behavior="padding"
        enabled
        keyboardVerticalOffset={-500}>
        <Image
          source={require('../assets/image/mln.png')}
          style={styles.background}
        />
        <View style={styles.input}>
          <Text style={styles.title1}>Welcome,</Text>
          <Text style={styles.title2}>Please Login First</Text>
          <Text style={styles.email}>Email</Text>
          <TextInput
            placeholder="Email"
            style={styles.emailInput}
            caretHidden={false}
            keyboardType="email-address"
          />
          <Text style={styles.password}>Password</Text>
          <TextInput
            placeholder="Password"
            secureTextEntry={true}
            style={styles.passwordInput}
          />
          <View style={styles.backgroundButton}>
            <View style={styles.buttonLogo}>
              <TouchableOpacity>
                <Image
                  source={require('../assets/icon/gmail.png')}
                  style={styles.gmail}
                />
              </TouchableOpacity>
              <TouchableOpacity>
                <Image
                  source={require('../assets/icon/Facebook.png')}
                  style={styles.facebook}
                />
              </TouchableOpacity>
              <TouchableOpacity>
                <Image
                  source={require('../assets/icon/twitter.png')}
                  style={styles.twitter}
                />
              </TouchableOpacity>
            </View>
            <TouchableOpacity
              style={styles.buttonForgot}
              onPress={() => navigation.navigate('Reset')}>
              <Text style={styles.forgotText}>Forgot Password?</Text>
            </TouchableOpacity>
          </View>
          <TouchableOpacity
            style={styles.loginButton}
            onPress={() => navigation.navigate('PhoneQuestion')}>
            <Text style={styles.loginText}>Login</Text>
          </TouchableOpacity>
          <View style={styles.registerNavigate}>
            <Text style={styles.registerQuestion}>Don't Have An Account?</Text>
            <TouchableOpacity onPress={() => navigation.navigate('Register')}>
              <Text style={styles.registerText}>Register</Text>
            </TouchableOpacity>
          </View>
        </View>
      </KeyboardAvoidingView>
      {/* </ScrollView> */}
    </View>
  );
};

export default Login;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    marginBottom: 40,
  },
  background: {
    width: Dimensions.get('window').width, //atur agar lebar gambar adalah selebar layar device
    height: 270,
  },
  title1: {
    fontSize: 30,
    fontWeight: 'bold',
    color: '#0A0827',
  },
  title2: {
    fontSize: 30,
    fontWeight: 'bold',
    color: '#0A0827',
  },
  input: {
    width: '100%',
    backgroundColor: '#fff',
    borderTopLeftRadius: 19,
    borderTopRightRadius: 19,
    paddingHorizontal: 20,
    paddingTop: 38,
    marginTop: -20,
  },
  email: {
    color: '#335EFF',
    fontWeight: 'bold',
    marginTop: 25,
  },
  emailInput: {
    marginTop: 11,
    width: '100%',
    borderRadius: 8,
    backgroundColor: '#F6F8FF',
    paddingHorizontal: 10,
  },
  password: {
    color: '#335EFF',
    fontWeight: 'bold',
    marginTop: 11,
  },
  passwordInput: {
    marginTop: 11,
    width: '100%',
    borderRadius: 8,
    backgroundColor: '#F6F8FF',
    paddingHorizontal: 10,
  },
  backgroundButton: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 15,
    justifyContent: 'space-between',
  },
  buttonLogo: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  gmail: {
    width: 36,
    height: 25,
    resizeMode: 'contain',
  },
  facebook: {
    width: 36,
    height: 25,
    marginHorizontal: 15,
    resizeMode: 'contain',
  },
  twitter: {
    width: 36,
    height: 25,
    resizeMode: 'contain',
  },
  buttonForgot: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  forgotText: {
    fontSize: 12,
    color: '#717171',
  },
  loginButton: {
    width: '100%',
    marginTop: 77,
    backgroundColor: '#335EFF',
    borderRadius: 8,
    paddingVertical: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },
  loginText: {
    color: '#fff',
    fontSize: 16,
    fontWeight: 'bold',
  },
  registerNavigate: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 30,
    flexDirection: 'row',
  },
  registerQuestion: {
    fontSize: 12,
    color: '#717171',
  },
  registerText: {
    fontSize: 14,
    color: '#335EFF',
    marginLeft: 5,
  },
});
